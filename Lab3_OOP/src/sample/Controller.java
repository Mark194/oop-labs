package sample;

import data.*;

import java.util.*;
import javafx.collections.*;

import javafx.collections.ObservableList;

import javafx.scene.control.*;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import message.Message;
import message.MessageAlert;
import parser.ParserData;
import parser.ParserText;


public class Controller {
    Message mes = new MessageAlert();
    Boolean[] status = new Boolean[12];
    ParserData parser = new ParserText();
    Boolean firstStatus = false;
    private HashMap <String, ObservableList<String>> lines = new HashMap <String, ObservableList<String>>();
    @FXML
    public TabPane tabPane;
    @FXML
    AnchorPane pane1;
    @FXML
    private TextField RecordEdit;

    @FXML
    public void showGame() throws Exception{
        Stage stage = new Stage();
        Main main = new Main();
        main.createScene(stage);
    }

    @FXML
    private void initialize() {
        if (mes.showConfirmation("Загрузить файл из директории?").getResult() == ButtonType.YES) {
            OpenText op = new OpenTextFile();
            op.open();
            if (op.countLoad() != 0) {
                LoadData load = new LoadDataToLines();
                load.setData(op.getData());
                this.lines = load.load();
                firstStatus = true;
            } else {
                mes.showError("Невозможно загрузить файлы!");
            }

        }

        for (int i = 0; i < tabPane.getTabs().size(); i++) {
            Tab tab = tabPane.getTabs().get(i);
            AnchorPane pane = (AnchorPane)tab.getContent();
            ListView listview = ((ListView)pane.getChildren().get(0));
            String name = tab.getText();
            listview.setItems(lines.get(name));
        }
        setStatus();

    }

    public void addListener(ListView listview){
        listview.getItems().addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(ListChangeListener.Change c) {
                int position = 0;
                for (Tab tab : tabPane.getTabs()) {
                    AnchorPane pane = (AnchorPane) tab.getContent();
                    ListView listview = (ListView) pane.getChildren().get(0);
                    if ((ObservableList<String>)c.getList() != null) {
                        if (listview.getItems().equals((ObservableList<String>) c.getList())) {
                            status[position] = true;
                            break;
                        }
                    }
                        else {
                        status[position] = false;
                    }
                    position++;
                }
            }
        });
    }

    @FXML
    public void addListItems() {
        AnchorPane pane = (AnchorPane)checkTabActive().getContent();
        ListView listview = (ListView)pane.getChildren().get(0);
        addItems(listview);
    }

    public Tab checkTabActive(){
        return tabPane.getTabs().get(tabPane.getSelectionModel().getSelectedIndex());
    }

    public void addItems(ListView example) {
        ObservableList<String> result = parser.parseTextField(RecordEdit.getText(), parser.regular);
        if (result.size() !=0) {
            if (checkDublicate(example, delDublicate(result)))
                mes.showError("Содержит повторяющиеся строки!");
            else {
                if (example.getItems() == null) {
                    example.setItems(result);
                    addListener(example);
                    status[tabPane.getSelectionModel().getSelectedIndex()] = true;
                }
                else {
                    example.getItems().addAll(result);
                    status[tabPane.getSelectionModel().getSelectedIndex()] = true;
                }
            }
        } else {
            mes.showError("Пожалуйста, введите корректные данные!");
        }
    }


    public boolean checkDublicate(ListView example, ObservableList<String> result) {
        boolean d = false;
        if (example.getItems() != null && result.size() > 0) {
            for (String line : result)
                for (String line2 : (ObservableList<String>) example.getItems())
                    if (line.equals(line2))
                        d = true;
        }
        return d;
    }

    public ObservableList<String> delDublicate(ObservableList<String> result) {
        if (result.size() > 1)
        for (String line : result)
            for (String line2 : result)
                if (line.equals(line2) && result.indexOf(line) != result.indexOf(line2))
                    result.remove(line2);
                return result;
    }

    public void changeListItems() {
        AnchorPane pane = (AnchorPane)checkTabActive().getContent();
        ListView listview = (ListView)pane.getChildren().get(0);
        changeItems(listview);
    }

    public void changeItems(ListView example) {
        int position = example.getSelectionModel().getSelectedIndex();

        String newLine = RecordEdit.getText();

        if (checkPosition(position)) {
            if (newLine != "" && !example.getItems().contains(newLine)) {
                example.getItems().set(position, newLine) ;
                status[tabPane.getSelectionModel().getSelectedIndex()] = true;
            } else
                mes.showError("Некорректное изменение!");
        }
    }

    public void delListItems() {
        AnchorPane pane = (AnchorPane)checkTabActive().getContent();
        ListView listview = (ListView)pane.getChildren().get(0);
        delItems(listview);
    }

    public void delItems(ListView example) {
        int position = example.getSelectionModel().getSelectedIndex();
        if (checkPosition(position)) {
            example.getItems().remove(position);
            example.getSelectionModel().select(position);
        }
    }

    public void delAllListItems() {
        AnchorPane pane = (AnchorPane)checkTabActive().getContent();
        ListView listview = (ListView)pane.getChildren().get(0);
        delAllItems(listview);
    }

    public void delAllItems(ListView example) {
        if (example.getItems().toString() != "[]"){
            if (mes.showConfirmation("Вы действительно хотите удалить всё?").getResult() == ButtonType.YES){
                example.getItems().removeAll(example.getItems());
            } else {
                mes.showInformation("Удаление отменено!");
            }
        } else {
            mes.showError("Отсутствую строки!");
        }
    }


    public boolean checkPosition(int position) {
        if (position < 0) {
            mes.showError("Элемент не выбран!");
            return false;
        } else
            return true;
    }

    public void clickOnListView() {
        AnchorPane pane = (AnchorPane)checkTabActive().getContent();
        ListView listview = (ListView)pane.getChildren().get(0);
        changeText(listview);
    }

    public void changeText(ListView example) {
        if (example.getSelectionModel().getSelectedIndex() != -1)
            RecordEdit.setText(example.getSelectionModel().getSelectedItem().toString());
    }

    public void createLines(){
        for (int i = 0; i < tabPane.getTabs().size(); i++){
            AnchorPane ap = (AnchorPane)tabPane.getTabs().get(i).getContent();
            ListView listview = (ListView)ap.getChildren().get(0);
            if (listview.getItems() != null)
                lines.put(tabPane.getTabs().get(i).getText(),listview.getItems());
        }
    }

    public void saveChanges(){
        check();
        if (checkStatus()){
            if (mes.showConfirmation("Сохранить изменения?").getResult() == ButtonType.YES) {
                createLines();
                SaveText save = new SaveTextFile(lines);
                save.save(tabPane, status);
                setStatus();
                System.exit(0);
            } else
                System.exit(0);
        } else {
            System.exit(0);
        }
    }

    public boolean checkStatus() {
        if (Arrays.asList(status).contains(true))
            return true;
        else
            return false;
    }

    public void setStatus(){
        for (int i = 0; i<status.length; i++)
            status[i] = false;
        firstStatus = false;
    }

    public void check(){
        for (int i = 0; i < tabPane.getTabs().size(); i++){
            Tab tab = tabPane.getTabs().get(i);
            AnchorPane ap = (AnchorPane)tab.getContent();
            ListView listview = (ListView)ap.getChildren().get(0);
            if (listview.getItems() == null || listview.getItems().size() == 0) {
                status[i] = false;
                firstStatus = false;
            }
            else {
                for (ObservableList<String> collection : lines.values())
                if (listview.getItems().equals(collection))
                    status[i] = false;
            }
        }
    }



}
