package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.scene.input.KeyEvent;
import javafx.stage.WindowEvent;

import java.io.File;

public class Main extends Application {
    File f = new File(".");
    Scene scene;
    Scene scene2;
    Controller cd;
    Stage primaryStage2;
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Лабораторная работа №3");
        primaryStage.getIcons().add(new Image("file:"+f.getAbsolutePath()+"\\resources\\notepad.png"));
        scene = new Scene(root, 896, 419);
        cd = loader.getController();
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().toString() == "ENTER") {
                    cd.addListItems();
                }
            }
        });
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                cd.saveChanges();
            }
        });
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void createScene(Stage primaryStage2) throws Exception {
        FXMLLoader loader = new FXMLLoader(gallows.Controller.class.getResource("form2.fxml"));
        Parent root = loader.load();
        primaryStage2.setTitle("Игра \"Виселица\"");
        primaryStage2.getIcons().add(new Image("file:"+f.getAbsolutePath()+"\\resources\\gallows.png"));
        scene = new Scene(root, 687, 393);
        primaryStage2.setScene(scene);
        primaryStage2.show();
    }

    @Override

    public void stop(){
        cd.saveChanges();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public Controller getController(){
        return this.cd;
    }
}
