package entity;

import java.util.ArrayList;

public class Word {
    private ArrayList<Symbol> symbols = new ArrayList<>();

    public ArrayList<Symbol> getSymbols(){
        return this.symbols;
    }

    public Word(String line){
        for (int i = 0; i < line.length(); i++){
            this.symbols.add(i, new Symbol(line.charAt(i)));
        }
    }

    @Override
    public String toString(){
        String tempLine = "";
        for (Symbol temp : symbols){
            tempLine +=temp.getSymbol();
        }
        return tempLine;
    }

}
