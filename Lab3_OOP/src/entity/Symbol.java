package entity;

public class Symbol {
    private boolean secret;
    private char symbol;

    public Symbol(char symbol){
        this.secret = true;
        this.symbol = symbol;
    }

    public char getSymbol() {
        if (!this.secret)
            return symbol;
        else
            return '*';
    }

    public boolean checkSymbol(char d){
        if (d == symbol)
            return true;
        else
            return false;
    }

    public void setSecret(boolean secret){
        this.secret = secret;
    }
}
