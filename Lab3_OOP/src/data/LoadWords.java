package data;

import message.Message;
import message.MessageAlert;
import parser.ParserData;
import parser.ParserText;
import sun.misc.IOUtils;

import javax.swing.text.html.parser.Parser;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class LoadWords  {

    Message mes =  new MessageAlert();
    ArrayList<String> words = new ArrayList<>();
    File file = new File("resources\\slova.txt");
    String name= file.getAbsolutePath();
    String temp = "";

    public void open() {
        String temp;
        File f = new File(name);
        BufferedReader reader;
        try  {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
            while ( (temp = reader.readLine()) != null) {
                words.add(temp);
            }
        } catch (FileNotFoundException e) {
            mes.showError("Cann't open file!");
        } catch (IOException e) {
            mes.showError("Input/output error!");
        } finally
        {
        mes.showInformation("Слова загружены!");
        }
    }

    public ArrayList<String> getWords(){
        return words;
    }

    public String getRandomWords(){
        int min = 0;
        int max = words.size();
        Random rnd = new Random(System.currentTimeMillis());
        int number = min + rnd.nextInt(max - min + 1);
        return words.get(number);
    }

    public void setWord(){
        ParserData parser = new ParserText();
        words.addAll(parser.parseTextField(temp, parser.regularWords));
    }


}
