package data;

import javafx.scene.control.TabPane;
import javafx.stage.Stage;

import java.io.FileNotFoundException;

public interface SaveText {
    void save(TabPane tabPane, Boolean[] status );
}
