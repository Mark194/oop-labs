package data;

public interface OpenText {
    void open();
    String[] getData();
    int countLoad();
}
