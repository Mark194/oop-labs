package data;

import javafx.collections.ObservableList;
import parser.ParserData;
import parser.ParserText;
import java.util.HashMap;

public class LoadDataToLines implements LoadData {
    private HashMap <String, ObservableList<String>> lines =  new HashMap<String, ObservableList<String>>();
    private String [] data;
    String[] months = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Декабрь"};

    public void setData(String[] data) {
        this.data = data;
    }

    public HashMap <String, ObservableList<String>> load() {
        for (int i = 0; i < data.length; i++){
            if (data[i].length() > 0) {
                lines.put(months[i], loadData(data[i]));
            }
        }
        return lines;
    }

    public ObservableList<String> loadData(String line){
        ParserData parser = new ParserText();
        return parser.parseTextField(line, parser.regular);
    }



}
