package data;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import message.Message;
import message.MessageAlert;

import java.io.*;

public class OpenTextFile implements OpenText {
    Message mes = new MessageAlert();
    String[] result = new String[12];
    private int countLoad = 0;

    public String[] getData(){
        return this.result;
    }

    private void setString(){
        for (int i = 0; i < result.length; i++)
            result[i]="";
    }
    @Override
    public void open() {
        String folder = "";
        setString();
        FileChooser directoryChooser = new FileChooser();
        directoryChooser.setTitle("Выберите текстовый файл.");
        directoryChooser.setInitialDirectory(new File("C:\\Users\\zheny\\Desktop\\"));
        File dir = directoryChooser.showOpenDialog(null);
        if (dir != null) {
            folder = dir.getAbsolutePath();
            openText(folder);
        }
    }

    public void openText(String name) {
        String temp = "";
        File f = new File(name);
        FileReader reader = null;
        try  {
            reader = new FileReader(f);
            int i = 0;
            while(reader.ready() && i < 12) {
                if(temp.contains("\r\n") ){
                    result[i] = temp;
                    i++;
                    temp = "";
                }
                else
                    temp += (char)reader.read();
            }
            countLoad ++;
            mes.showInformation("Файл успешно считан!");
        } catch (FileNotFoundException e) {
            mes.showError("Cann't open file!");
        } catch (IOException e) {
            mes.showError("Input/output error!");
        }
    }

    public int countLoad() {
        return countLoad;
    }
}
