package data;

import java.io.*;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.DirectoryChooser;
import message.Message;
import message.MessageAlert;

public class SaveTextFile implements SaveText {
    HashMap <String, ObservableList<String>> data;
    Message mes = new MessageAlert();

    public SaveTextFile(HashMap<String, ObservableList<String>> lines) {
        this.data = lines;
    }

    @Override
    public void save(TabPane tabPane, Boolean[] status) {
        String folder = "";
        String temp = "";
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Выберите директорию");
        directoryChooser.setInitialDirectory(new File("C:\\Users\\zheny\\Desktop\\"));
        File dir = directoryChooser.showDialog(null);
        if (dir != null) {
            folder = dir.getAbsolutePath();
        }
            for (int i = 0; i < 12; i++) {
                String name = tabPane.getTabs().get(i).getText();
                if (status[i] == true) {
                    ObservableList<String> example = data.get(name);
                    temp += collectionToString(example);
                }
            }
        writeText(temp, folder, "text_modified.txt");
    }

    public void writeText(String line, String folder, String name){
        try (FileWriter fos = new FileWriter (folder+"\\"+name)) {
            fos.write(line);
            mes.showInformation("Запись в файл выполнена");
        } catch (FileNotFoundException e) {
            mes.showError("Cann't open file!");
        } catch(IOException e){
            mes.showError("Ошибка ввода/вывода");
        }
    }

    public String collectionToString(ObservableList<String> example){
        String result = "";
        if (example.size() != 0) {
            for (String line : example)
                result += line + ", ";
        }
        result += "\r\n";
        return result;
    }
}
