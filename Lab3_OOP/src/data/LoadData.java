package data;

import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.HashMap;

public interface LoadData {
    HashMap<String, ObservableList<String>> load();
    void setData(String[] data);
}
