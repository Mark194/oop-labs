package parser;

import javafx.collections.ObservableList;
import javafx.scene.control.TextField;

public interface ParserData {
    public String regular = "(\\\"+(.*?)+\\\"+[,\\s])|([^\\\"^\\s]+[A-zА-я0-9]+[,\\s])";
    public String regularWords = "[A-zА-я]+";
    ObservableList<String> parseTextField(String line, String regular);
}
