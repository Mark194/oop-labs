package parser;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserText implements ParserData {



    public ObservableList<String> parseTextField(String line, String regular) {
        ObservableList<String> result = FXCollections.observableArrayList();
        Pattern p = Pattern.compile(regular);
        Matcher m = p.matcher(line);
        while (m.find())
            result.add(delCharofLines(m.group()));
        return result;
    }

    private String delCharofLines(String line) {
        if (line.contains("\""))
            line = line.substring(1, line.length() - 2);
        else
            line = line.substring(0, line.length() - 1);
        return line;
    }
}
