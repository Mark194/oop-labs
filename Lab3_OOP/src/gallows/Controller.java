package gallows;

import data.LoadWords;
import entity.Symbol;
import entity.Word;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import message.Message;
import message.MessageAlert;

public class Controller {
    @FXML
    Label secretWord, countLabel;
    Word word;
    Message mes = new MessageAlert();
    @FXML
    Canvas canvas;

    @FXML
    AnchorPane pane;

    int count = 0;
    LoadWords load;
    @FXML
    private void initialize() {
        load = new LoadWords();
        load.open();
        load.setWord();
        secretWord.setText(encryptionString(load.getRandomWords()));
        countLabel.setText(Integer.toString(count));
    }

    private void drawGallows(int n){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        switch(n){
            case 1: gc.strokeLine(0, canvas.getHeight(), canvas.getWidth(), canvas.getHeight());  break;
            case 2: gc.strokeLine(10, 0, 10, 300);  break;
            case 3: gc.strokeLine(0, 0, (int)canvas.getWidth()-100, 0);  break;
            case 4: gc.strokeLine((int)canvas.getWidth()-130, 0, (int)canvas.getWidth()-130, 50);  break;
            case 5: gc.strokeOval((int)canvas.getWidth()-150, 50, 40, 40); break;
            case 6: gc.strokeOval((int)canvas.getWidth()-150, 90, 40, 60); break;
            case 7: gc.strokeLine((int)canvas.getWidth()-146, 101, (int)canvas.getWidth()-160, 150);  break;
            case 8: gc.strokeLine((int)canvas.getWidth()-100, 150, (int)canvas.getWidth()-114, 101);  break;
            case 9: gc.strokeLine((int)canvas.getWidth()-114, 200,(int)canvas.getWidth()-128, 151);  break;
            case 10: gc.strokeLine((int)canvas.getWidth()-128, 151,(int)canvas.getWidth()-144, 200);
            default: if (n >=10) { mes.showInformation("Вы проиграли!.."+"\r\n"); break; }
        }

    }

    private String encryptionString(String line){
        word = new Word(line);
        return word.toString();
    }


    @FXML
    public void changeWord(ActionEvent e) {
        Button but = ((Button) e.getSource());
        String b = but.getText();
        if (!checkSecret() && count<10) {
            if (checkSymbol(b.charAt(0)))
                but.setVisible(false);
            else {
                but.setDisable(true);
                but.setStyle(" -fx-background-color: red;  -fx-border-color: silver; -fx-border-radius: 10%; -fx-opacity: 12.0;");
            }
        }
    }

    private boolean checkSymbol(char symbol){
        boolean b = false;
        for (Symbol d : word.getSymbols()){
            if (d.checkSymbol(symbol)) {
                d.setSecret(false);
                secretWord.setText(word.toString());
                b = true;
            }
        }
        if (!b && count<10) {
            count++;
            drawGallows(count);
        }
        countLabel.setText(Integer.toString(count));
        return b;
    }

    private boolean checkSecret(){
        boolean f = false;
        int count = 0;
        for (Symbol d : word.getSymbols()){
            if (!(d.getSymbol() == '*'))
                count++;
        }
        if (count == word.getSymbols().size()) {
            f = true;
            mes.showInformation("Вы выиграли!");
        }
        return f;
    }

    @FXML
    public void restart(){
        count = 0;
        secretWord.setText(encryptionString(load.getRandomWords()));
        countLabel.setText(Integer.toString(count));
        for(int i = 0; i < 33; i++) {
            Button but = (Button)pane.getChildren().get(i+2);
            but.setDisable(false);
            but.setVisible(true);
            but.setStyle(" -fx-background-color: gainsboro;  -fx-border-color: silver; -fx-border-radius: 10%; -fx-opacity: 12.0;");
        }
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

}
