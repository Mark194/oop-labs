package data;

import filter.Filter;
import filter.FileFilter;
import javafx.stage.FileChooser;
import message.Message;
import message.MessageAlert;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LoadFile implements LoadData {

    private Message mes = new MessageAlert();
    private File file;
    private String text;

    @Override
    public void load() {
        Filter filter = new FileFilter();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(filter.getFilter());
        fileChooser.setTitle("Выберите файл");
        try {
            file = fileChooser.showOpenDialog(null);
            file.getAbsolutePath();
            readFile();
        } catch (NullPointerException e){
            mes.showError("Файл не выбран!");
        }
    }

    public void readFile(){
        try  {
            text = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
            mes.showInformation("Файл успешно считан.");
        } catch (FileNotFoundException e) {
            mes.showError("Cann't open file!");
        } catch (IOException e) {
            mes.showError("Input/output error!");
        }
        setTagBR();
    }

    public File getFile() {
        return file;
    }

    public String getText() {
        return text;
    }

    public void setTagBR(){
        if (text.contains("\r\n"))
            text = text.replaceAll("\r\n","<br>");
    }
}
