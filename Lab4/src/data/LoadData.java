package data;

import java.io.File;

public interface LoadData {
    void load();
    File getFile();
    String getText();
}
