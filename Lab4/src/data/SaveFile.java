package data;

import filter.FileFilter;
import filter.Filter;
import javafx.stage.FileChooser;
import message.Message;
import message.MessageAlert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class SaveFile implements SaveData {
    Message mes = new MessageAlert();
    private String text;
    File file;

    public SaveFile(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public void save() {
        FileChooser fileChooser = new FileChooser();
        Filter filter = new FileFilter();
        fileChooser.getExtensionFilters().addAll(filter.getFilter());
        fileChooser.setTitle("Выберите файл");
        try {
            file = fileChooser.showSaveDialog(null);
            writeFile();
        } catch (NullPointerException e){
            mes.showError("Файл не выбран!");
        }
    }

    public void writeFile(){
        FileWriter fileWriter = null;
        try  {
            fileWriter = new FileWriter(file);
            fileWriter.write(text);
        } catch (FileNotFoundException e) {
            mes.showError("Cann't open file!");
        } catch (IOException e) {
            mes.showError("Input/output error!");
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
