package sample;

import com.sun.webkit.WebPage;
import data.LoadData;
import data.LoadFile;
import data.SaveData;
import data.SaveFile;
import dialog.Windows;
import filter.WebViewEditorListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.GridPane;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import message.Message;
import message.MessageAlert;
import org.jsoup.Jsoup;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Optional;

public class Controller {
    WebView webView;
    Windows windows = new Windows();
    Dialog dialog = windows.showDialogFindAndReplace("Найти и заменить");
    ArrayList<StringBuilder> lines = new ArrayList <>();
    Message message = new MessageAlert();
    File file;
    Button backButton;
    String line;
    @FXML
    private MenuItem create, open, save, quit, back, cut, copy, paste, find, replace;
   @FXML
   public HTMLEditor htmlEditor;

   @FXML
   public void newText(){
       saveUpdates();
       htmlEditor.setHtmlText("");
   }

   @FXML
   public void openFile(){
        LoadData loadData = new LoadFile();
        loadData.load();
        file = loadData.getFile();
        webView.getEngine().loadContent(loadData.getText());
    }

    @FXML void saveFile(){
        SaveData saveData;
        if (message.showConfirmation("Сохранить стили?").getResult() == ButtonType.NO)
            saveData = new SaveFile(delHtmlTag(htmlEditor.getHtmlText()));
        else
            saveData = new SaveFile(htmlEditor.getHtmlText());
            saveData.save();
    }

    @FXML
    private void initialize() {
        create.setAccelerator(KeyCombination.valueOf("Ctrl+N"));
        open.setAccelerator(KeyCombination.valueOf("Ctrl+O"));
        save.setAccelerator(KeyCombination.valueOf("Ctrl+S"));
        quit.setAccelerator(KeyCombination.valueOf("Ctrl+Q"));
        back.setAccelerator(KeyCombination.valueOf("Ctrl+Z"));
        cut.setAccelerator(KeyCombination.valueOf("Ctrl+X"));
        copy.setAccelerator(KeyCombination.valueOf("Ctrl+C"));
        paste.setAccelerator(KeyCombination.valueOf("Ctrl+V"));
        find.setAccelerator(KeyCombination.valueOf("Ctrl+F"));
        replace.setAccelerator(KeyCombination.valueOf("Ctrl+R"));
   }

    @FXML
    public void exit(){
        saveUpdates();
        System.exit(0);
    }

    @FXML
    public void cut(){
        Button butCut = (Button)htmlEditor.lookup(".html-editor-cut");
        butCut.fire();
    }

    @FXML
    public void copy(){
        Button butCopy = (Button)htmlEditor.lookup(".html-editor-copy");
        butCopy.fire();
    }

    @FXML
    public void paste(){
        Button butPaste = (Button)htmlEditor.lookup(".html-editor-paste");
        butPaste.fire();
    }

    @FXML
    public void back() {
       if (lines.size() > 0) {
           line = del(line);
           String oldLine = lines.get(lines.size()-1).toString();
           oldLine = del(oldLine);
           String html = del(htmlEditor.getHtmlText());
           html = html.replace(line, oldLine);
           webView.getEngine().loadContent(html);
           line = lines.get(lines.size()-1).toString();
           lines.remove(lines.size()-1);
           if (lines.size() == 0)
               backButton.setDisable(true);
       }
    }

    @FXML
    public void find(){
        Windows windows = new Windows();
        Optional<String> result = windows.showDialogFind("Что:","Найти","Найти далее");
        try {
            if (result.isPresent()) {
                search(result.get());
                find();
            }
        } catch (NullPointerException e) {
            windows.getDialog().close();
        }

    }

    @FXML
    public void replace(){
        dialog.showAndWait();
        GridPane gridPane = (GridPane) dialog.getDialogPane().getChildren().get(3);
        TextField textField = (TextField) (gridPane.getChildren().get(1));
        TextField textField1 = (TextField) (gridPane.getChildren().get(3));
        if (!textField.getText().equals(textField1.getText())){
            findAndReplace(textField.getText(),textField1.getText());
        }
    }

    public void findAndReplace(String oldValue, String newValue){
        WebEngine engine = webView.getEngine();
        try {
            String html = htmlEditor.getHtmlText().replaceFirst(oldValue, newValue);
            Field pageField = engine.getClass().getDeclaredField("page");
            pageField.setAccessible(true);
            webView.getEngine().loadContent(html);
        } catch(Exception e) { /* log error could not access page */ }
    }


    public void search(String line){
        WebEngine engine = webView.getEngine();
        try {
            Field pageField = engine.getClass().getDeclaredField("page");
            pageField.setAccessible(true);
            WebPage page = (com.sun.webkit.WebPage) pageField.get(engine);
            page.find(line, true, true, false);
        } catch(Exception e) { /* log error could not access page */ }
    }



    public String delHtmlTag(String html) {
        return Jsoup.parse(html).text();
    }

    public void saveUpdates(){
        if (delHtmlTag(htmlEditor.getHtmlText()).length() > 0)
            if (message.showConfirmation("Сохранить изменения?").getResult() == ButtonType.YES)
                saveFile();
    }

    public void delElement () {
        webView = (WebView) htmlEditor.lookup(".web-view");
        ToolBar toolbar = (ToolBar) htmlEditor.lookup(".top-toolbar");
        ToolBar toolbar2 = (ToolBar) htmlEditor.lookup(".bottom-toolbar");
        toolbar.getItems().remove(7, 17);
        toolbar2.getItems().remove(0);
        toolbar2.getItems().remove(6,9);
        addButtonsToToolbar(toolbar);
        WebView webView = (WebView) htmlEditor.lookup(".web-view");
        WebViewEditorListener webViewEditorListener = new WebViewEditorListener(webView, new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (oldValue != null) {
                    if (lines.size() == 10)
                        lines.remove(0);
                    lines.add(new StringBuilder(oldValue));
                    line = newValue;
                    if (lines.size() == 0) {
                        backButton.setDisable(true);
                    } else {
                        backButton.setDisable(false);
                    }
                }
            }
        });

    }

    public void addButtonsToToolbar(ToolBar toolbar){

        Button butnew = new Button();
        Button butOpen = new Button();
        Button butExit = new Button();
        backButton = new Button();
        Button butFind = new Button();
        Button butSave = new Button();
        Button butReplace = new Button();
        Button[] buttons = {butnew, butOpen, butExit, backButton, butFind, butSave, butReplace};
        String[] styles = {"butNew", "butOpen", "butExit", "butBack", "butSave", "butFind", "butReplace"};
        String style = "button-pane";
        for (int i = 0; i < buttons.length; i++){
            buttons[i].getStyleClass().add(styles[i]);
            buttons[i].getStyleClass().add(style);
        }
        butnew.setOnMouseClicked(event -> newText());
        butExit.setOnMouseClicked(event -> exit());
        Separator separator = new Separator();
        Separator separator2 = new Separator();
        butSave.setOnMouseClicked(event -> saveFile());
        butFind.setOnMouseClicked(event -> find());
        butReplace.setOnMouseClicked(event -> replace());
        toolbar.getItems().add(separator);
        toolbar.getItems().add(0,butnew);
        toolbar.getItems().add(1,butOpen);
        toolbar.getItems().add(2,butSave);
        toolbar.getItems().add(3,butExit);
        toolbar.getItems().add(4,separator2);
        toolbar.getItems().add(5,backButton);
        toolbar.getItems().add(butFind);
        toolbar.getItems().add(butReplace);
        backButton.setDisable(true);
        backButton.setOnMouseClicked(event -> back());
    }

    public String del(String line){
       return line.replace("&nbsp;"," ");
    }

}
