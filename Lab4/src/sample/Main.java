package sample;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import screne.MainStart;

import java.io.File;


public class Main extends Application {
    Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Текстовый редактор");
        Scene scene = new Scene(root, 640, 400);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image("file:"+new File(".").getAbsolutePath()+"\\resources\\image\\notepad.png"));
        controller = loader.getController();
        primaryStage.setOnShown(new EventHandler <WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                controller.delElement();
            }
        });
        primaryStage.show();
    }

    public static void main(String[] args) {
        LauncherImpl.launchApplication(Main.class, MainStart.class, args);
    }

    @Override
    public void init() throws Exception{
        this.notifyPreloader(new Preloader.ProgressNotification(0.0));
        Thread.sleep(5000);
        this.notifyPreloader(new Preloader.ProgressNotification(1.0));
    }
}


