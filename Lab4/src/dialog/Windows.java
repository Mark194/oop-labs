package dialog;


import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import java.util.Optional;

public class Windows {

    private TextInputDialog textInputDialog;
    private ButtonBar buttonBar;



    public TextInputDialog getDialog(){
        return textInputDialog;
    }


    public Windows(){
        textInputDialog = new TextInputDialog();
    }

    public Optional<String> showDialogFind(String label, String title, String textBut){
        textInputDialog.setTitle(title);
        textInputDialog.setHeaderText(null);
        textInputDialog.setContentText(label);
        ButtonBar buttonBar = (ButtonBar)(textInputDialog.getDialogPane().getChildren().get(2));
        ((Button)buttonBar.getButtons().get(0)).setText(textBut);
        ((Button)buttonBar.getButtons().get(1)).setText("Отменить");
        Optional<String> result = textInputDialog.showAndWait();
        if (result.isPresent())
            return result;
        else
            return null;
    }

    public Dialog<Pair<String, String>> showDialogFindAndReplace(String title){
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle(title);

        ButtonType loginButtonType = new ButtonType("Заменить", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        TextField searchWord = new TextField();
        TextField replaceWord = new TextField();

        grid.add(new Label("Найти:"), 0, 0);
        grid.add(searchWord, 1, 0);
        grid.add(new Label("Заменить:"), 0, 1);
        grid.add(replaceWord, 1, 1);

        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        searchWord.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });
        dialog.getDialogPane().setContent(grid);
        return dialog;
    }

    public Dialog<Pair<String, String>> showDialogPassword(String title){
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle(title);

        ButtonType loginButtonType = new ButtonType("ОК", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        PasswordField passwordField = new PasswordField();
        grid.add(new Label("Пароль:"), 0, 0);
        grid.add(passwordField, 1, 0);
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        //loginButton.setDisable(true);
        dialog.getDialogPane().setContent(grid);
        return dialog;
    }

    public ButtonBar getButtonBar() {
        return buttonBar;
    }
}
