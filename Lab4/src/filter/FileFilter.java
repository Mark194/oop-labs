package filter;

import javafx.stage.FileChooser;

import java.util.ArrayList;

public class FileFilter implements Filter {
    ArrayList<FileChooser.ExtensionFilter> filters;

    public FileFilter (){
        filters = new ArrayList <>();
        this.filters.add(new FileChooser.ExtensionFilter("TXT files (*.txt)","*.txt"));
        this.filters.add( new FileChooser.ExtensionFilter("PAS files (*.pas)","*.pas"));
        this.filters.add(new FileChooser.ExtensionFilter("DPR file (*.dpr)","*.dpr"));
        this.filters.add( new FileChooser.ExtensionFilter("All files (*.*)", "*.*"));
    }

    @Override
    public ArrayList<FileChooser.ExtensionFilter> getFilter() {
        return filters;
    }
}
