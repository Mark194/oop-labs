package filter;

import javafx.stage.FileChooser;

import java.util.ArrayList;

public interface Filter {
    public ArrayList<FileChooser.ExtensionFilter> getFilter();
}
